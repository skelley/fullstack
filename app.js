const express = require("express")
const path = require("path")
const app = express()
const publicFolderPath = path.join(__dirname, "public")

app.use(express.json())
app.use(express.static(publicFolderPath))

let users = [];
app.post('/api/user', (req, res) => {
    const user = {
        name       : req.body.name,
        password   : req.body.password,
        job        : req.body.job,
        age        : req.body.age,
        mobilePhone: req.body.mobilePhone,
        homepage   : req.body.homepage,
        com        : req.body.com,
        devices    : req.body.devices,
        admin      : req.body.admin,
        select     : req.body.select,
        slide      : req.body.slide
    };
    
    if (users.some(user => {
        return user.name === req.body.name
    })) {
        res.status(409)
        res.send()
        return console.log("error")
    } else {
        users.push(user);
        res.status(201) //successful status code
        res.send(user);
    }
})

app.get('/api/user', (req, res) => {
    res.send(users)
});

// add POST request listener here
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}...`))






